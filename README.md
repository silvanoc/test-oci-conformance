# Test OCI conformance

Test conformance of the GitLab container registry with the OCI Distribution spec.

Results of the tests:

| Pull | Report |
| --- | --- |
| ![](https://gitlab.com/silvanoc/test-oci-conformance/badges/test-pull/pipeline.svg) | https://gitlab.com/silvanoc/test-oci-conformance/pipelines/test-pull/latest |

| Push | Report |
| --- | --- |
| ![](https://gitlab.com/silvanoc/test-oci-conformance/badges/test-push/pipeline.svg) | https://gitlab.com/silvanoc/test-oci-conformance/pipelines/test-push/latest |

| Content Discovery | Report |
| --- | --- |
| ![](https://gitlab.com/silvanoc/test-oci-conformance/badges/test-content-discovery/pipeline.svg) | https://gitlab.com/silvanoc/test-oci-conformance/pipelines/test-content-discovery/latest |

| Content Management | Report |
| --- | --- |
| ![](https://gitlab.com/silvanoc/test-oci-conformance/badges/test-content-management/pipeline.svg) | https://gitlab.com/silvanoc/test-oci-conformance/pipelines/test-content-management/latest |
